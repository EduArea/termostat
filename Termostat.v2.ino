#include <LiquidCrystal.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <EEPROM.h>
#include "bee-termo.h"

#define POWER_SYNC_PIN 2
//#define POWER_SYNC_PIN 3 //only for SparkFun Pro micro 16 injection
#define POWER_CTRL_PIN 13
//#define POWER_CTRL_PIN 2 //only for SparkFun Pro micro 16 injection
#define ONEWIREBUS_PIN 6

#define BTN_DOWN 5
#define BTN_UP   4
#define BTN_SET  3
//#define BTN_SET  15 //only for SparkFun Pro micro 16 injection

OneWire oneWire(ONEWIREBUS_PIN);
LiquidCrystal lcd(7, 8, 9, 10, 11, 12);
//LiquidCrystal lcd(7, 8, 9, 10, 16, 14); //only for SparkFun Pro micro 16 injection

DallasTemperature	sensors(&oneWire);
DeviceAddress		insideThermometer;

Preferences			EEPROM_Prefs; //То, что читается и пишется в EEPROM

volatile uint8_t	syncCount;
volatile uint8_t	impulses = 0;
volatile uint8_t	period = 100;

volatile uint8_t	targetPower;
volatile uint8_t	divider;

volatile float		highEdgeTemp;
volatile float		lastTemp;
volatile float		prevTemp;
volatile float		lowEdgeTemp;

volatile bool		newSync;
volatile bool		switch_on;

void powerSync() {
	if (digitalRead(POWER_SYNC_PIN) == HIGH) {    // поймано прерывание, когда состояние пина изменено
		if (!newSync)                             // если оно стало HIGH, то нам нужно инициировать новый счётный шаг
			newSync = true;                       // определяем, реагировали ли мы именно на это изменение (убираем ложные срабатывания)
		return;
	}
	if (!newSync) {                               // если это не новый счётный шаг, то мы просто прекращаем работу прерывания
		return;
	}

	if (!switch_on || isnan(lastTemp)) {                             // если по какой-то причине устройство должно отключить нагрузку (например, отрыв датчика), мы это делаем и прекращаем работу прерывания
		if (digitalRead(POWER_CTRL_PIN) == HIGH) {
			digitalWrite(POWER_CTRL_PIN, LOW);
		}
		newSync = false; return;
	}
	syncCount += 1; newSync = false;              // увеличить шаг счётного алгоритма и отметить, что мы этот шаг так или иначе обработаем. Он отныне "не новый шаг"
	float dTemp = highEdgeTemp - lastTemp;
	if (dTemp < 0) {                             // перегрев. Отключаемся. 
		if (digitalRead(POWER_CTRL_PIN) == HIGH)
			digitalWrite(POWER_CTRL_PIN, LOW);
		return;
	}
	else if (syncCount > period) {              // достигли конца счётного цикла
		syncCount = 0;                            // сбросить счётчик цикла
		impulses = targetPower;            // установить, сколько импульсов за следующий период следует выставлять в высокое состояние
		if (lastTemp > lowEdgeTemp)
			impulses = impulses / divider;
	}
	if (impulses > 0) {                          // если за цикл ещё остались "неизрасходованные" патроны "высокого" состояния контрольного пина
		if (digitalRead(POWER_CTRL_PIN) == LOW)
			digitalWrite(POWER_CTRL_PIN, HIGH);
		impulses -= 1;
	}
	else {
		if (digitalRead(POWER_CTRL_PIN) == HIGH)
			digitalWrite(POWER_CTRL_PIN, LOW);
	}
}

void setup() {
	switch_on = false;
	noInterrupts();
	syncCount = 0;
	Serial.begin(9600);

	lcd.begin(16, 2);

	pinMode(POWER_SYNC_PIN, INPUT_PULLUP);
	pinMode(POWER_CTRL_PIN, OUTPUT);
	pinMode(BTN_DOWN, INPUT_PULLUP);
	pinMode(BTN_UP, INPUT_PULLUP);
	pinMode(BTN_SET, INPUT_PULLUP);

	readFromEEPROM();

	attachInterrupt(0, powerSync, CHANGE);
	interrupts();

	switch_on = true;
}

void readFromEEPROM() {
	EEPROM.get(0, EEPROM_Prefs);
	if (isnan(EEPROM_Prefs.highEdgeTemp))			// NAN будет в том случае, если не удалось прочитать корректно float
		EEPROM_Prefs.highEdgeTemp = 5.0;			// установить 5 градусов C если температура в EEPROM не число 
	if (EEPROM_Prefs.power < 1 || EEPROM_Prefs.power > period)
		EEPROM_Prefs.power = period / 2;
	if (EEPROM_Prefs.divider < 1 || EEPROM_Prefs.divider > 10)
		EEPROM_Prefs.divider = 10;
	if (isnan(EEPROM_Prefs.lowEdgeTemp))
		EEPROM_Prefs.lowEdgeTemp = EEPROM_Prefs.highEdgeTemp - 2.0;     // Если не удалось прочитать корректно float из EEProm
											// мы будем считать, что нижняя граница срабатывания будет равна 2.0 градуса
	highEdgeTemp = EEPROM_Prefs.highEdgeTemp;
	targetPower = EEPROM_Prefs.power;
	lowEdgeTemp = EEPROM_Prefs.lowEdgeTemp;
	divider = EEPROM_Prefs.divider;
}

long lastSensorReadTime;

// возвратит NAN если датчика нет
float getTemp() {
	lastSensorReadTime = millis();
	sensors.requestTemperatures();
	if (!sensors.getAddress(insideThermometer, 0)) {
		return NAN;
	}
	else
		return sensors.getTempCByIndex(0);
}

String getTempString(float value, bool shft) {
	if (isnan(value))
		return "???";
	return (shft && value<100?" ":"") + String(value, 1) + String(char(0x99));
}

String getPowerString(uint8_t val,bool bkt) {
	String pwrStr = bkt ? "[" : " ";
	pwrStr += val > 9 ? "" : " ";
	pwrStr += String(val) + "%";
	pwrStr += bkt ? "]" : " ";
	return pwrStr;
}


volatile bool DOWN_BTN;
volatile bool UP_BTN;
volatile bool SET_BTN;

long last_BTN_readTime;
volatile bool changeState = false;

void clearSetBtn() {
	changeState = false;
	last_BTN_readTime = millis();
}

void readButtons() {
	long currentTime = millis();
	long delta = abs(currentTime - last_BTN_readTime);
	if (delta > 499 || (delta > 199 && !SET_BTN)) {
		DOWN_BTN = !digitalRead(BTN_DOWN);
		UP_BTN = !digitalRead(BTN_UP);
		SET_BTN = !digitalRead(BTN_SET);
		if (SET_BTN && !changeState)
			changeState = true;
		if (!DOWN_BTN && !UP_BTN && !SET_BTN)
			return;
		last_BTN_readTime = currentTime;
	}
}

void readUpDown() {
	switch (state) {
	default:
		return;
	case (SETHIGHTEMP) :
		if (DOWN_BTN && highEdgeTemp > 1)
			highEdgeTemp -= 0.1;
		else if (UP_BTN && highEdgeTemp < 125)
			highEdgeTemp += 0.1;
		if (lowEdgeTemp > highEdgeTemp)
			lowEdgeTemp = highEdgeTemp - 2.0;
		return;
	case (SETPOWER) :
		if (DOWN_BTN && targetPower > 1)
			targetPower -= 1;
		else if (UP_BTN && targetPower < period)
			targetPower += 1;
		return;
	case (SETLOWTEMP) :
		if (DOWN_BTN && lowEdgeTemp > 0)
			lowEdgeTemp -= 0.5;
		else if (UP_BTN && lowEdgeTemp < highEdgeTemp)
			lowEdgeTemp += 0.5;
		return;
	case (SETDIVIDER) :
		if (DOWN_BTN && divider > 1)
			divider -= 1;
		else if (UP_BTN && divider < 10)
			divider += 1;
		return;
	}
}

void displayHighEdgeTemp() {
	placeString(0, 0, F("Bepx\xbd\xc7\xc7 \xb4pa\xbd\xb8\xe5\x61:"));
	placeString(0, 1, "      " + getTempString(highEdgeTemp, true));
}

void displayLowEdgeTemp() {
	placeString(0, 0, F("H\xb8\xb6\xbd\xc7\xc7 \xb4pa\xbd\xb8\xe5\x61:"));
	placeString(0, 1, "    " + getTempString(lowEdgeTemp, true));
}

void displaySetPower() {
	placeString(0, 0, "Mo\xe6\xbd\x6f\x63\xbf\xc4:       ");
	placeString(0, 1, "    " + getPowerString(targetPower, true) + "  ");
}

bool clearScreen;

void targetTempControl() {
	long currentTime = millis();
	if (abs(currentTime - lastSensorReadTime) > 619) {
		float sensValue = getTemp();
		if (isnan(sensValue)) {
			switch_on = false;
			state = TEMPWARNING;
			warningStatus = F("    BH\xa5MAH\xa5\x45!   ");
			warningMessage = F(" - \xbd\x65\xbf \xe3\x61\xbf\xc0\xb8\xba\x61 - ");
		}
		else {
			prevTemp = lastTemp;
			lastTemp = sensValue;
			if (lastTemp > prevTemp) {
				tempState = RISE;
			}
			else if (lastTemp < prevTemp) {
				tempState = FALL;
			}
			else if (lastTemp == prevTemp)
				tempState = NOCHANGES;
			switch (state) {
			default:
				return;
			case NORMAL:
				if (highEdgeTemp < lastTemp) {
					switch_on = false;
					tempState = OVER;
					state = WAIT;
				}
				break;
			case WAIT:
				if (lastTemp < lowEdgeTemp) {
					switch_on = true;
					state = NORMAL;
				}
				else
					tempState = INZONE;
				break;
			case TEMPWARNING:
				state = WAIT;
				clearScreen = true;
				break;
			}
		}
	}
}

void saveEEPROM(bool force) {
	if (force || (highEdgeTemp != EEPROM_Prefs.highEdgeTemp) ||
		(targetPower != EEPROM_Prefs.power) ||
		(lowEdgeTemp != EEPROM_Prefs.lowEdgeTemp)) {
		EEPROM_Prefs.highEdgeTemp = highEdgeTemp;
		EEPROM_Prefs.power = targetPower;
		EEPROM_Prefs.lowEdgeTemp = lowEdgeTemp;
		EEPROM_Prefs.divider = divider;
		EEPROM.put(0, EEPROM_Prefs);
	}
}

void placeString(int x, int y, String value) {
	lcd.setCursor(x, y);
	lcd.print(value);
}

long lastDisplayTime;


String getTempBehaviour() {
	switch (tempState) {
	case (RISE) :
		return F("\xd9\xd9\xd9\xd9");
	case (FALL) :
		return F("\xda\xda\xda\xda");
	case (NOCHANGES) :
		return F("....");
	case (OVER) :
		return F("!!!!");
	case (INZONE) :
		return F("\xb6\xe3\xb5\xbc");
	default:
		return F("????");
	}
}

void repaintLCD() {
	long currentTime = millis();
	if (abs(currentTime - lastDisplayTime) < 197) {
		return;
	}
	lastDisplayTime = currentTime;
	if (clearScreen) {
		lcd.clear();
	}
	switch (state) {
	default:
	case (NORMAL) :
		if (prevTemp != lastTemp || clearScreen) {
			placeString(0, 1, getTempString(lowEdgeTemp, false));
			placeString(10, 1, getTempString(highEdgeTemp, true));
			bool chPwr = (lastTemp < highEdgeTemp) && (lastTemp > lowEdgeTemp);
			placeString(0, 0, getPowerString(targetPower, !chPwr && state==NORMAL));
			placeString(5, 0, getPowerString((targetPower/divider), chPwr && state == NORMAL));
			placeString(10, 0, getTempString(lastTemp, true));
		}
		if (prevTemp != lastTemp)
			placeString(6, 1, getTempBehaviour());
		break;
	case (TEMPWARNING) :
		placeString(0, 0, warningStatus);
		placeString(0, 1, warningMessage);
		break;
	case (SETHIGHTEMP) :
		readUpDown();
		displayHighEdgeTemp();
		break;
	case (SETLOWTEMP) :
		readUpDown();
		displayLowEdgeTemp();
		break;
	case (SETPOWER) :
		readUpDown();
		displaySetPower();
		break;
	case (SETDIVIDER) :
		readUpDown();
		placeString(0, 0, "\xe0\x65\xbb\xb8\xbf\x65\xbb\xc4:");
		placeString(10, 0, String(divider) + " ");
		placeString(0, 1, " " + getPowerString(targetPower, false) + " --> " + getPowerString(targetPower / divider, true) + " ");
		break;
	}
	if (clearScreen)
		clearScreen = !clearScreen;
}

long lastChangeState;

void switchBySetButton() {
	if (!changeState)
		return;

	switch (state) {
	default:
		return;
	case (NORMAL) :
	case (WAIT) :
		if (changeState) {
			state = SETHIGHTEMP;
			clearSetBtn();
			clearScreen = true;
			switch_on = false;
		}
		break;
	case (SETHIGHTEMP) :
		if (changeState) {
			state = SETLOWTEMP;
			clearSetBtn();
			clearScreen = true;
		}
		break;
	case (SETLOWTEMP) :
		if (changeState) {
			state = SETPOWER;
			clearSetBtn();
			clearScreen = true;
		}
		break;
	case (SETPOWER) :
		if (changeState) {
			state = SETDIVIDER;
			clearSetBtn();
			clearScreen = true;
		}
		break;
	case (SETDIVIDER) :
		if (changeState) {
			state = WAIT;
			clearSetBtn();
			clearScreen = true;
			saveEEPROM(false);
		}
		break;
	}
}

void loop() {
	readButtons();
	switchBySetButton();
	targetTempControl();
	repaintLCD();
}
