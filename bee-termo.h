//
// Created by Stanislav Lakhtin on 05/05/15.
//

#ifndef TERMOSTAT_V2_BEE_TERMO_H
#define TERMOSTAT_V2_BEE_TERMO_H

enum STATE { NORMAL, WAIT, SETHIGHTEMP, SETPOWER, SETLOWTEMP, SETDIVIDER, TEMPWARNING };
enum TEMPERATURE {RISE, FALL, NOCHANGES, OVER, INZONE};
STATE state;
TEMPERATURE tempState;

String warningStatus;
String warningMessage;

struct Preferences {
	float		highEdgeTemp;
	uint8_t		power;
	float		lowEdgeTemp;
	uint8_t		divider;
};

#endif //TERMOSTAT_V2_BEE_TERMO_H
